FROM php:7.3-apache-buster

ENV PORT 8080

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    inetutils-syslogd \
    ca-certificates \
    curl \
    wget \
    zip \
    unzip \
    locales

# Remove apt cache
RUN apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync
RUN install-php-extensions gd \
    curl \
    intl \
    json \
    xml \
    bcmath \
    mbstring \
    zip \
    sqlite3 \
    apcu

RUN docker-php-ext-install mysqli pdo_mysql exif

RUN install-php-extensions curl

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ports.conf /etc/apache2/ports.conf
COPY start.sh /usr/local/bin/start

RUN chmod u+x /usr/local/bin/start

RUN a2enmod rewrite

# Start docker
CMD ["/usr/local/bin/start"]
